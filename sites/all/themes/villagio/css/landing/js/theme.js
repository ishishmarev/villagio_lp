(function ($) {
  $(document).ready(function(){


    if ( $('input[name="submitted[e_mail]"]').length > 0 ) {
      $('input[name="submitted[e_mail]"]').inputmask({
        alias: "email",
        "placeholder": "_",
        clearMaskOnLostFocus: true,
        showMaskOnHover: false,
        showMaskOnFocus: true
      });
    }
    if ( $('input[name="submitted[telefon]"]').length > 0 ) {
      $('input[name="submitted[telefon]"]').inputmask("phoneru", {
        "placeholder": "_",
        clearMaskOnLostFocus: true,
        showMaskOnHover: false,
        showMaskOnFocus: true
      });
    }
    // Drupal.behaviors.webform_inputmask = {
    //   attach: function (context, settings) {
    //     $('input[name="submitted[e_mail]"]').inputmask({
    //       alias: "email",
    //       "placeholder": "_",
    //       clearMaskOnLostFocus: true,
    //       showMaskOnHover: false,
    //       showMaskOnFocus: true
    //     });
    //     $('input[name="submitted[telefon]"]').inputmask("phoneru", {
    //       "placeholder": "_",
    //       clearMaskOnLostFocus: true,
    //       showMaskOnHover: false,
    //       showMaskOnFocus: true
    //     });
    //   }
    // };

    send_my_webform('.block-webform', 'div.form-submit');
    // Drupal.behaviors.webform_client_form = {
    //   attach: function (context, settings) {
    //     send_my_webform('.webform-client-form-3', 'div.form-submit');
    //   }
    // };
    function send_my_webform(form, my_input) {
      jQuery(form).find(my_input).on('click', function(){
        jQuery(form).find('.form-actions input.form-submit').click();
      });
      jQuery(form).find('input.required.error').first().focus();
    }

    scroll_top_page_to_id_block('.description__location', 0);
    function scroll_top_page_to_id_block(id_block, offset_top){
      $(id_block + ' a[href^="#"]').click(function(){
        var el = $(this).attr('href');
        $('body, html').animate({
          scrollTop: $(el).offset().top - offset_top}, 700);
        return false;
      });
    }

    if ( $('.promo__slider').length > 0 ) {
      $('.promo__slider').slick({
        dots: true,
        arrows: true,
        infinite: true,
        autoplay: false,
        pauseOnHover: false,
        pauseOnFocus: false,
        autoplaySpeed: 3000,
        variableWidth: false,
        speed: 1000,
        fade: true,
        adaptiveHeight: false,
        vertical: false,
        verticalSwiping: false,
        slidesPerRow: 0,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.promo__arrow_prev'),
        nextArrow: $('.promo__arrow_next')
      });
    }
    if ( $('.additional-slider_slider-1').length > 0 ) {
      $('.additional-slider_slider-1 .additional-slider__tabs-content-inner').slick({
        dots: true,
        arrows: true,
        infinite: true,
        autoplay: false,
        pauseOnHover: false,
        pauseOnFocus: false,
        autoplaySpeed: 3000,
        variableWidth: false,
        speed: 1000,
        fade: true,
        adaptiveHeight: true,
        vertical: false,
        verticalSwiping: false,
        slidesPerRow: 0,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.additional-slider_slider-1 .additional-slider__arrow_prev'),
        nextArrow: $('.additional-slider_slider-1 .additional-slider__arrow_next')
      });
    }
    if ( $('.additional-slider_slider-2').length > 0 ) {
      $('.additional-slider_slider-2 .additional-slider__tabs-content-inner').slick({
        dots: true,
        arrows: true,
        infinite: true,
        autoplay: false,
        pauseOnHover: false,
        pauseOnFocus: false,
        autoplaySpeed: 3000,
        variableWidth: false,
        speed: 1000,
        fade: true,
        adaptiveHeight: true,
        vertical: false,
        verticalSwiping: false,
        slidesPerRow: 0,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.additional-slider_slider-2 .additional-slider__arrow_prev'),
        nextArrow: $('.additional-slider_slider-2 .additional-slider__arrow_next')
      });
    }

    if ( $('.additional-slider__tabs').length > 0 ) {
      $('.additional-slider__tabs li').first().addClass('active');
      $('.additional-slider__tabs li').on('click', function(){
        var datalavel = $(this).data('level');
        $('.additional-slider__tabs li').removeClass('active');
        $(this).addClass('active');
        $('.additional-slider_slider-1 .additional-slider__tabs-content .slide').each(function(){
          if ( $(this).data('level') == datalavel ) {
            $('.additional-slider_slider-1 .additional-slider__tabs-content-inner').slick('slickGoTo', $(this).eq(0).index());
          }
        });
      });
    }



  });
})(jQuery);
