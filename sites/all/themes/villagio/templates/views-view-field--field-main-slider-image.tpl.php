<?php foreach ($row->field_field_main_slider_image as $item_id => $item) :
  $image_url = file_create_url($item['raw']['uri']);
  global $base_url;
  if (strpos($image_url, $base_url) === 0) {
    $image_url = '/' . ltrim(str_replace($GLOBALS['base_url'], '', $image_url), '/');
  }
  $query_string = dechex(time());
?>
<div class="sp-slide">
  <span class="slide-image" style="background-image: url('<?php print $image_url . '?' . $query_string; ?>');"></span>
</div>
<?php endforeach; ?>