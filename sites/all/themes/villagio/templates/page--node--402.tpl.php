<link rel="stylesheet" href="/sites/all/themes/villagio/css/landing/css/bootstrap.min.css">
<link rel="stylesheet" href="/sites/all/themes/villagio/css/landing/css/animate.min.css">
<link rel="stylesheet" href="/sites/all/themes/villagio/css/landing/css/lightgallery.min.css">
<link rel="stylesheet" href="/sites/all/themes/villagio/css/landing/css/slick.css">
<link rel="stylesheet" href="/sites/all/themes/villagio/css/landing/css/main.css">

<div class="promo">
      <div class="promo__arrow promo__arrow_prev">
         
        <svg xmlns="http://www.w3.org/2000/svg" width="37" height="65" viewBox="0 0 37 65"><path fill="#d7d7d7" d="M36.3 61.75L33 65 0 32.5 33 0l3.3 3.25L6.6 32.5z"/><path fill="#fff" d="M36.3 61.75L33 65 0 32.5 33 0l3.3 3.25L6.6 32.5z"/></svg>
      </div>
      <div class="promo__arrow promo__arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="37" height="65" viewBox="0 0 37 65"><path fill="#d7d7d7" d="M.7 61.75L4 65l32.99-32.5L4 0 .7 3.25 30.4 32.5z"/><path fill="#fff" d="M.7 61.75L4 65l32.99-32.5L4 0 .7 3.25 30.4 32.5z"/></svg></div>
      <div class="promo__slider">
        <div class="promo__slide promo__slide-1">
          <div class="promo__container container">
            <div class="row">
              <div class="col-xs-20 col-md-10 col-lg-12">
                <div class="promo__title">Ваш дом для отдыха в 2 строки и даже в 3 строки</div>
                <div class="promo___text">Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы. Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы</div>
                <div class="promo__button button button__type-1 button__b"><a href="#">Узнать цену</a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="promo__slide promo__slide-2">
          <div class="promo__container container">
            <div class="row">
              <div class="col-xs-20 col-md-10 col-lg-12">
                <div class="promo__title">Ваш дом для отдыха в 2 строки</div>
                <div class="promo___text">Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы. Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы</div>
                <div class="promo__button button button__type-1 button__b"><a href="#">Узнать цену</a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="promo__slide promo__slide-3">
          <div class="promo__container container">
            <div class="row">
              <div class="col-xs-20 col-md-10 col-lg-12">
                <div class="promo__title">Ваш дом для отдыха</div>
                <div class="promo___text">Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы. Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы</div>
                <div class="promo__button button button__type-1 button__b"><a href="#">Узнать цену</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <section class="application-form application-form_top">
      <div class="application-form-top__container container">
        <div class="block-webform">
          <h2>Форма заявки</h2>
          <div class="form-item webform-component webform-component-markup webform-component--prosto-tekst">Лесные просторы, живописные луга, озера – в Подмосковье есть то очарование природы, которого так не хватает жителям столицы.</div>
          <?php print render(drupal_get_form('villagio_core_form')); ?>
        </div>
      </div>
    </section>
    <div class="navbar navbar_type-4">
      <div class="navbar__container container">
        <div class="navbar__main-menu">
          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="23" viewBox="0 0 40 23">
            <path d="M0 3V0h40v3zm0 10v-3h40v3zm0 10v-3h40v3z"></path>
          </svg>
        </div>
        <div class="navbar__phone">+8 800 245-45-45</div>
      </div>
    </div>
    <section class="description description_type-4">
      <div class="description__container container">
        <div class="row">
          <div class="col-xs-20 col-md-15">
            <div class="description__title">Невероятно уютный и комфортный особняк <br/> в скандинавском стиле в самом сердце соснового бора</div>
            <div class="description__location"> <a href="#map">24 км от МКАД, Рублево-Успенское шоссе, <br/>Посёлок «Миллениум Парк»</a></div>
            <ul>
              <li class="description__item-1">15 соток</li>
              <li class="description__item-2">580 м2</li>
              <li class="description__item-3">15 соток</li>
              <li class="description__item-4">01.12.2017</li>
              <li class="description__item-5">3 спальни</li>
              <li class="description__item-6">01.12.2017</li>
            </ul>
          </div>
          <div class="col-xs-20 col-md-5">
            <div class="description__id"><span>ID дома:</span><span>5555 </span></div>
            <div class="description__prices">
              <div class="prices__label">Цена сейчас:</div><span class="prices__prefix">$</span><span class="prices__price">2 500 000</span>
            </div>
            <div class="description__end-construction">
              <div class="end-construction__label">Цена по окончанию <br/>строительства:</div>
              <div class="end-construction__prices"><span class="end-construction__prefix">$</span><span class="end-construction__price">3 500 000</span></div>
            </div>
            <div class="description__button button button__type-4 button__s"><a href="#">Узнать реальную цену</a></div>
          </div>
        </div>
      </div>
    </section>
    <section class="dop-buttons dop-buttons_type-6">
      <div class="dop-buttons__container container">
        <div class="dop-buttons__send-request dop-buttons__button button button__type-2 button__b"><a href="#">Отправить заявку</a></div>
      </div>
    </section>
    <section class="additional-slider additional-slider_slider-1">
      <div class="additional-slider__tabs">
        <div class="additional-slider__container container">
          <ul>
            <li data-level="inside-1">1 уровень</li>
            <li data-level="inside-2">2 уровень</li>
            <li data-level="outside">Дополнительно</li>
          </ul>
        </div>
      </div>
      <div class="additional-slider__tabs-content">
        <div class="additional-slider__container container">
          <div class="additional-slider__arrow additional-slider__arrow_prev"> 
            <svg xmlns="http://www.w3.org/2000/svg" width="33" height="60" viewBox="0 0 37 65">
              <path fill="#d7d7d7" d="M36.3 61.75L33 65 0 32.5 33 0l3.3 3.25L6.6 32.5z"></path>
              <path fill="#fff" d="M36.3 61.75L33 65 0 32.5 33 0l3.3 3.25L6.6 32.5z"></path>
            </svg>
          </div>
          <div class="additional-slider__arrow additional-slider__arrow_next">
            <svg xmlns="http://www.w3.org/2000/svg" width="33" height="60" viewBox="0 0 37 65">
              <path fill="#d7d7d7" d="M.7 61.75L4 65l32.99-32.5L4 0 .7 3.25 30.4 32.5z"></path>
              <path fill="#fff" d="M.7 61.75L4 65l32.99-32.5L4 0 .7 3.25 30.4 32.5z"></path>
            </svg>
          </div>
        </div>
        <div class="additional-slider__tabs-content-inner">
          <div data-level="inside-1" class="slide slide-1">
            <div class="tabs-content__container container">
              <div class="slide__content">
                <div class="text col-xs-20 col-md-8">
                  <p>На первом этаже –  котельная, гараж, ванная комната, кухня-гостиная с выходом на террасу.</p>
                </div>
              </div>
            </div>
          </div>
          <div data-level="inside-1" class="slide slide-2">
            <div class="tabs-content__container container">
              <div class="slide__content">
                <div class="text col-xs-20 col-md-8">
                  <p>Интерьер оформлен в теплой бежево-коричневой гамме. В отделке использованы натуральные материалы - дерево и камень.</p>
                </div>
              </div>
            </div>
          </div>
          <div data-level="inside-2" class="slide slide-3">
            <div class="tabs-content__container container">
              <div class="slide__content">
                <div class="text col-xs-20 col-md-8">
                  <p>На втором этаже – две спальни с ванными комнатами, гардеробная, два балкона и лоджия.</p>
                </div>
              </div>
            </div>
          </div>
          <div data-level="inside-2" class="slide slide-4">
            <div class="tabs-content__container container">
              <div class="slide__content">
                <div class="text col-xs-20 col-md-8">
                  <p>Зеркальное изголовье кровати не только выглядит очень стильно, но визуально расширяет пространство и добавляет света.</p>
                </div>
              </div>
            </div>
          </div>
          <div data-level="outside" class="slide slide-5">
            <div class="tabs-content__container container">
              <div class="slide__content">
                <div class="text col-xs-20 col-md-8">
                  <p>Дом монолитный - несущий скелет сделан из цельного бетона. Благодаря этому здание прекрасно сохраняет тепло и обеспечивает шумоизоляцию.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="dop-buttons dop-buttons_type-6">
      <div class="dop-buttons__container container">
        <div class="dop-buttons__send-request dop-buttons__button button button__type-2 button__b"><a href="#">Отправить заявку</a></div>
      </div>
    </section>
    <section class="additional-slider additional-slider_slider-2">
      <div class="additional-slider__container container">
        <h2>Посёлок «Миллениум Парк»</h2>
      </div>
      <div class="additional-slider__tabs-content">
        <div class="container">
          <div class="additional-slider__arrow additional-slider__arrow_prev"> 
            <svg xmlns="http://www.w3.org/2000/svg" width="37" height="65" viewBox="0 0 37 65">
              <path fill="#d7d7d7" d="M36.3 61.75L33 65 0 32.5 33 0l3.3 3.25L6.6 32.5z"></path>
              <path fill="#fff" d="M36.3 61.75L33 65 0 32.5 33 0l3.3 3.25L6.6 32.5z"></path>
            </svg>
          </div>
          <div class="additional-slider__arrow additional-slider__arrow_next">
            <svg xmlns="http://www.w3.org/2000/svg" width="37" height="65" viewBox="0 0 37 65">
              <path fill="#d7d7d7" d="M.7 61.75L4 65l32.99-32.5L4 0 .7 3.25 30.4 32.5z"></path>
              <path fill="#fff" d="M.7 61.75L4 65l32.99-32.5L4 0 .7 3.25 30.4 32.5z"></path>
            </svg>
          </div>
        </div>
        <div class="additional-slider__tabs-content-inner">
          <div data-level="inside-1" class="slide slide-1">
            <div class="tabs-content__container container">
              <div class="slide__content">
                <div class="text col-xs-20 col-md-8"> 
                  <p>Вечнозелёная растительность, чистый воздух с нотками хвойи. Находиться здесь приятно в любое время года.</p>
                  <p>Инфраструктура:</p>
                  <ul>
                    <li>магазин</li>
                    <li>ресторан</li>
                    <li>фитнес-центр</li>
                    <li>детский сады</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div data-level="inside-1" class="slide slide-2">
            <div class="tabs-content__container container">
              <div class="slide__content">
                <div class="text col-xs-20 col-md-8">Интерьер оформлен в теплой бежево-коричневой гамме. В отделке использованы натуральные материалы - дерево и камень.</div>
              </div>
            </div>
          </div>
          <div data-level="inside-2" class="slide slide-3">
            <div class="tabs-content__container container">
              <div class="slide__content">
                <div class="text col-xs-20 col-md-8">На втором этаже – две спальни с ванными комнатами, гардеробная, два балкона и лоджия.</div>
              </div>
            </div>
          </div>
          <div data-level="inside-2" class="slide slide-4">
            <div class="tabs-content__container container">
              <div class="slide__content">
                <div class="text col-xs-20 col-md-8">Зеркальное изголовье кровати не только выглядит очень стильно, но визуально расширяет пространство и добавляет света.</div>
              </div>
            </div>
          </div>
          <div data-level="outside" class="slide slide-5">
            <div class="tabs-content__container container">
              <div class="slide__content">
                <div class="text col-xs-20 col-md-8">Дом монолитный - несущий скелет сделан из цельного бетона. Благодаря этому здание прекрасно сохраняет тепло и обеспечивает шумоизоляцию.</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="dop-buttons dop-buttons_type-6">
      <div class="dop-buttons__container container">
        <div class="dop-buttons__send-request dop-buttons__button button button__type-2 button__b"><a href="#">Отправить заявку</a></div>
      </div>
    </section>
    <section id="map" class="map">
      <h2>Посёлок на карте</h2>
      <script type="text/javascript" charset="utf-8" async="" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A69eff6ea0f1096bc66d5f48128a4606ba26b1ce094017603bc5d1f8a83ef3c58&amp;width=100%25&amp;height=630&amp;lang=ru_RU&amp;scroll=true"></script>
    </section>
    <section class="dop-buttons dop-buttons_type-6">
      <div class="dop-buttons__container container">
        <div class="dop-buttons__send-request dop-buttons__button button button__type-2 button__b"><a href="#">Отправить заявку</a></div>
      </div>
    </section>
    <section class="other-home">
      <div class="other-home__container container">
        <h2>Другие объекты</h2>
        <div class="row">
          <?php print views_embed_view('view_villagio_core', 'panel_pane_1', 49); ?>
        </div>
      </div>
    </section>
    <section class="dop-buttons dop-buttons_type-6">
      <div class="dop-buttons__container container">
        <div class="dop-buttons__send-request dop-buttons__button button button__type-2 button__b"><a href="#">Отправить заявку</a></div>
      </div>
    </section>
    <section class="payments">
      <div class="payments__container container">
        <h2>Способы оплаты</h2>
        <div class="payments__text">Мы предлагаем гибкую схему системы оплаты. Вы можете осуществить <br/> платеж любым удобным способом</div>
        <ul class="payments__list">
          <li class="payments__item payments__item-1">
            <div>Разовый платеж</div>
          </li>
          <li class="payments__item payments__item-2">
            <div>Рассрочка</div>
          </li>
          <li class="payments__item payments__item-3">
            <div>Ипотека</div>
          </li>
        </ul>
      </div>
    </section>
    <section class="dop-buttons dop-buttons_type-6">
      <div class="dop-buttons__container container">
        <div class="dop-buttons__send-request dop-buttons__button button button__type-2 button__b"><a href="#">Отправить заявку</a></div>
      </div>
    </section>
    <section class="documentation">
      <div class="documentation__container container">
        <h2>Документация</h2>
        <div class="documentation__text">Мы предлагаем гибкую схему системы оплаты. Вы можете осуществить <br/>платеж любым удобным способом</div>
        <div class="documentation__list">
          <div class="documentation__item col-xs-10 col-md-4">
            <div class="documentation__image-wrapper">
              <div class="documentation__button button button__type-2 button__s"><a href="#">Скачать</a></div>
              <div class="documentation__button button button__type-2 button__s"><a href="#">Посмотреть</a></div>
            </div>
            <div class="documentation__title">Название документа 1 в 2 строки</div>
          </div>
          <div class="documentation__item col-xs-10 col-md-4">
            <div class="documentation__image-wrapper">
              <div class="documentation__button button button__type-2 button__s"><a href="#">Скачать</a></div>
              <div class="documentation__button button button__type-2 button__s"><a href="#">Посмотреть</a></div>
            </div>
            <div class="documentation__title">Название документа 1 в 2 строки</div>
          </div>
          <div class="documentation__item col-xs-10 col-md-4">
            <div class="documentation__image-wrapper">
              <div class="documentation__button button button__type-2 button__s"><a href="#">Скачать</a></div>
              <div class="documentation__button button button__type-2 button__s"><a href="#">Посмотреть</a></div>
            </div>
            <div class="documentation__title">Название документа 1 в 2 строки</div>
          </div>
          <div class="documentation__item col-xs-10 col-md-4">
            <div class="documentation__image-wrapper">
              <div class="documentation__button button button__type-2 button__s"><a href="#">Скачать</a></div>
              <div class="documentation__button button button__type-2 button__s"><a href="#">Посмотреть</a></div>
            </div>
            <div class="documentation__title">Название документа 1 в 2 строки</div>
          </div>
          <div class="documentation__item col-xs-10 col-md-4">
            <div class="documentation__image-wrapper">
              <div class="documentation__button button button__type-2 button__s"><a href="#">Скачать</a></div>
              <div class="documentation__button button button__type-2 button__s"><a href="#">Посмотреть</a></div>
            </div>
            <div class="documentation__title">Название документа 1 в 2 строки</div>
          </div>
        </div>
      </div>
    </section>
    <section class="application-form">
      <div class="application-form__container container">
        <div class="block-webform">
          <h2>Форма заявки</h2>
          <div class="form-item webform-component webform-component-markup webform-component--prosto-tekst">Лесные просторы, живописные луга, озера – в Подмосковье <br/>есть то очарование природы, которого так не хватает <br/>жителям столицы.</div>
          <?php print render(drupal_get_form('villagio_core_form')); ?>
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__container container">
        <div class="footer__copyright">2017 © Villagio. Все права защищены</div>
        <div class="footer__developers">Разработка сайта: <a href="#">Наименование дизайн-студии</a></div>
      </div>
    </footer>
    <script src="/sites/all/themes/villagio/css/landing/js/jquery.min.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/bootstrap.min.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/slick.min.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/wow.min.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/lightgallery.min.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/lg-thumbnail.min.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/lg-video.min.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/lg-zoom.min.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/lg-pager.min.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/inputmask.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/jquery.inputmask.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/inputmask.extensions.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/inputmask.phone.extensions.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/phone-ru.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/priceformat.min.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/classie.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/selectFx.js"></script>
    <script src="/sites/all/themes/villagio/css/landing/js/theme.js"></script>
    <script>
      (function($){
      	jQuery.fn.lightTabs = function(options){
      		var createTabs = function(){
      			tabs = this;
      			i = 0;
      			showPage = function(i){
      				$(tabs).find(".tabs-content").find(".tab-content").hide();
      				$(tabs).find(".tabs-content").find(".tab-content").eq(i).show();
      				$(tabs).find(".tabs").find("li").removeClass("active");
      				$(tabs).find(".tabs").find("li").eq(i).addClass("active");
      			}
      			showPage(0);
      			$(tabs).find(".tabs").find("li").each(function(index, element){
      				$(element).attr("data-page", i);
      				i++;
      			});
      			$(tabs).find(".tabs").find("li").click(function(){
      				showPage(parseInt($(this).attr("data-page")));
      			});
      		};
      		return this.each(createTabs);
      	};
      })(jQuery);
      $(document).ready(function(){
      	$(".additional-slider").lightTabs();
      });
    </script>