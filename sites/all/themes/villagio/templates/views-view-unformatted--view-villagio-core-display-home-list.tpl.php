<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

$home_list = $view->result;
$qwe = 1;
$main_title = $home_list[0]->field_field_home_list_main_title;
$home_list_items = $home_list[0]->field_field_home_list_set_list

//$home_list[0]->field_field_home_list_set_list
//$home_list[0]->field_field_home_list_main_title[0]['rendered']['#markup']

//            foreach ($slide_menu as $slide_menu_id => $slide_menu_item) :
?>

<div id="more" class="main__more container--fluid">
  <div class="container">
    <?php if ($main_title): ?>
    <h3 class="main__description--title small-text-center">
      <?php print $main_title[0]['rendered']['#markup']; ?>
    </h3>
    <?php endif; ?>

    <?php if (!empty($home_list_items)): ?>
    <div class="row">

      <div class="col-xs-30 col-sm-15">
        <?php
        foreach ($home_list as $item_id => $item) :

          $image = $item->field_field_home_list_image;
          if (empty($image)) {
            $image = $item->field_field_main_slider_image;
          }

          $image_url = file_create_url($image[0]['raw']['uri']);
          global $base_url;
          if (strpos($image_url, $base_url) === 0) {
            $image_url = '/' . ltrim(str_replace($GLOBALS['base_url'], '', $image_url), '/');
          }

          $title = $item->field_field_home_list_title;
          $description = $item->field_field_home_list_description;
          //$price_now = $item->field_field_price_now;
          $price_now = $item->field_field_home_list_price;
        ?>
          <?php if ($item_id%2 == 0): ?>

          <div class="main__more--item">
            <div class="more__item--image">
              <?php if (!empty($item->field_field_home_list_image)): ?>
              <img src="<?php print $image_url; ?>" alt="<?php print $image[0]['raw']['alt']; ?>" title="<?php print $image[0]['raw']['title']; ?>">
              <?php else: ?>
              <div class="bg" style="background-image: url('<?php print $image_url; ?>');"></div>
              <?php endif;?>
            </div>
            <div class="more__item--body">
              <?php if (!empty($title)): ?>
              <h3 class="main__description--title">
                <?php print $title[0]['raw']['value']; ?>
              </h3>
              <?php endif;?>

              <?php if (!empty($description)): ?>
              <div class="main__description--info">
                <span>
                  <?php print $description[0]['raw']['value']; ?>
                </span>
              </div>
              <?php endif;?>

              <?php if (!empty($price_now)): ?>
              <div class="main__description--price">
                <div class="description__price--label">
                  <span>Цена сейчас:</span>
                </div>
                <div class="description__price--count">
                  <span>
                    <?php print $price_now[0]['raw']['value']; ?>
                  </span>
                </div>
              </div>
              <?php endif;?>

              <div class="more__item--button">
                <a href="/node/<?php print $item->node_field_data_field_home_list_set_list_nid; ?>" class="button">
                  Посмотреть
                </a>
              </div>
            </div>
          </div>

          <?php endif; ?>
        <?php endforeach; ?>
      </div>
      <div class="col-xs-30 col-sm-15">
        <?php
        foreach ($home_list as $item_id => $item) :

          $image = $item->field_field_home_list_image;
          if (empty($image)) {
            $image = $item->field_field_main_slider_image;
          }

          $image_url = file_create_url($image[0]['raw']['uri']);
          global $base_url;
          if (strpos($image_url, $base_url) === 0) {
            $image_url = '/' . ltrim(str_replace($GLOBALS['base_url'], '', $image_url), '/');
          }

          $title = $item->field_field_home_list_title;
          $description = $item->field_field_home_list_description;
          //$price_now = $item->field_field_price_now;
          $price_now = $item->field_field_home_list_price;
          ?>
          <?php if ($item_id%2 != 0): ?>

          <div class="main__more--item">
            <div class="more__item--image">
              <?php if (!empty($item->field_field_home_list_image)): ?>
                <img src="<?php print $image_url; ?>" alt="<?php print $image[0]['raw']['alt']; ?>" title="<?php print $image[0]['raw']['title']; ?>">
              <?php else: ?>
                <div class="bg" style="background-image: url('<?php print $image_url; ?>');"></div>
              <?php endif;?>
            </div>
            <div class="more__item--body">
              <?php if (!empty($title)): ?>
                <h3 class="main__description--title">
                  <?php print $title[0]['raw']['value']; ?>
                </h3>
              <?php endif;?>

              <?php if (!empty($description)): ?>
                <div class="main__description--info">
                <span>
                  <?php print $description[0]['raw']['value']; ?>
                </span>
                </div>
              <?php endif;?>

              <?php if (!empty($price_now)): ?>
                <div class="main__description--price">
                  <div class="description__price--label">
                    <span>Цена сейчас:</span>
                  </div>
                  <div class="description__price--count">
                  <span>
                    <?php print $price_now[0]['raw']['value']; ?>
                  </span>
                  </div>
                </div>
              <?php endif;?>

              <div class="more__item--button">
                <a href="/node/<?php print $item->node_field_data_field_home_list_set_list_nid; ?>" class="button">
                  Посмотреть
                </a>
              </div>
            </div>
          </div>

        <?php endif; ?>
        <?php endforeach; ?>
      </div>

    </div>
    <?php else: ?>
    <p>Не указан список домов для вывода<p>
    <?php endif; ?>
  </div>
</div>
