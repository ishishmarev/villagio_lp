<div class="product col-xs-20 col-md-10">
  <div class="product__image-wrapper"><img src="http://savepic.net/9740878.png" alt=""></div>
  <div class="product__info">
    <div class="product__title"><?php print $fields['field_home_list_title']->content; ?></div>
    <div class="product__description"><?php print $fields['field_home_list_description']->content; ?></div>
    <?php if ($fields['field_price_now']->content) { ?>
    <div class="product__prices prices">

      <div class="prices__label">Цена сейчас:</div><!-- <span class="prices__prefix">$</span> --><span class="prices__price"><?php print $fields['field_price_now']->content; ?></span>
    </div>
    <?php } ?>
    <div class="product__button button button__type-2 button__s"><a href="#">Посмотреть</a></div>
  </div>
</div>