<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

$title = $fields['field_advantage_title']->content;
$subtitle = $fields['field_advantage_subtitle']->content;
$set = $fields['field_advantage_set']->content;
$home_id = $fields['field_home_id']->content;
$price_now_text = $fields['field_price_now_text']->content;
$price_now = $fields['field_price_now']->content;
$price_after_build_text = $fields['field_price_after_build_text']->content;
$price_after_build = $fields['field_price_after_build']->content;
?>

<div class="container">

  <div class="row">
    <div class="animate-04 col-sm-18 col-xs-30">
      <?php print $title; ?>
      <div class="animate-04">
        <?php print $subtitle; ?>
      </div>
    </div>
    <div class="animate-08 col-sm-12 col-xs-30 description-prices">
      <?php print $home_id; ?>
      <?php if ($price_now): ?>
        <div class="animate-04 main__description--price">
          <div class="description__price--label">
            <?php print $price_now_text; ?>
          </div>
          <div class="description__price--count">
          <span>
            <?php print $price_now; ?>
          </span>
          </div>
        </div>
      <?php endif; ?>

      <?php if ($price_after_build): ?>
        <div class="animate-08 main__description--afterprice">
          <div class="description__price--label">
            <?php print $price_after_build_text; ?>
          </div>
          <div class="description__price--count">
          <span>
            <?php print $price_after_build; ?>
          </span>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>

  <div class="animate-12">
    <?php print $set; ?>
  </div>
</div>