
<?php

// Plugin definition
$plugin = array(
    'title' => t('General layout'),
    'category' => t('Custom layouts'),
    'icon' => 'general.png',
    'theme' => 'general',
    'regions' => array(
        'main_content' => t('Main content')
    )
);