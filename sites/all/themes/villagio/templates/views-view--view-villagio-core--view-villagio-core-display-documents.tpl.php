<?php if (!empty($view->result[0]->field_field_documents_title) || !empty($view->result[0]->field_field_documents_subtitle) || !empty($view->result[0]->field_field_documents)): ?>
  <div id="documents" class="additional__slider container--fluid main__menu--container documents">
    <div class="container documents-wrapper">
      <?php print $rows; ?>
    </div>
  </div>
<?php endif; ?>