<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */

$documents = $row->field_field_documents;

?>


<?php
foreach ($documents as $item_id => $item) :
  $item_image_path = $item['rendered']['field_documents_image'][0]['#markup'];
  $item_image_alt = $item['rendered']['field_documents_image']['#items'][0]['alt'];
  $item_image_title =  $item['rendered']['field_documents_image']['#items'][0]['title'];
  $item_title = $item['rendered']['field_document_image_title'][0]['#markup'];
  $item_title_pdf = $item['rendered']['field_document_file'][0]['#markup'];
  ?>
  <div class="col-lg-6 col-md-6 col-sm-15 col-xs-30 noPadding">
    <div class="documents-item" style="background-image: url('<?php print $item_image_path; ?>');">
      <div class="documents-hide-btn">
        <a href="<?php print $item_title_pdf; ?>" class="button button-extra documents-item-link" target="_blank">Скачать</a>
        <a href="<?php print $item_title_pdf; ?>" class="button button-extra documents-item-link" target="_blank">Посмотреть</a>
      </div>
    </div>
    <div class="documents-name"><?php print $item_title; ?></div>
  </div>
<?php endforeach; ?>

