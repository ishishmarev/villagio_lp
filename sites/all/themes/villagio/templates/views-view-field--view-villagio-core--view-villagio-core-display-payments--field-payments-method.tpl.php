<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */

$advantage_set = $row->field_field_payments_method;

?>


<?php
foreach ($advantage_set as $item_id => $item) :
  $item_image_path = $item['rendered']['field_advantage_set_icon'][0]['#markup'];
  $item_image_alt = $item['rendered']['field_advantage_set_icon']['#items'][0]['alt'];
  $item_image_title =  $item['rendered']['field_advantage_set_icon']['#items'][0]['title'];
  $item_title = $item['rendered']['field_advantage_set_description'][0]['#markup'];
  ?>
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-30 noPadding">
    <div class="payments-item">
      <div class="main__description--item">
      <span class="main__description--icon">
        <img src="<?php print $item_image_path; ?>" alt="<?php print $item_image_alt; ?>" title="<?php print $item_image_title; ?>" />
      </span>
        <span>
        <?php print $item_title; ?>
      </span>
      </div>
    </div>
  </div>
<?php endforeach; ?>

