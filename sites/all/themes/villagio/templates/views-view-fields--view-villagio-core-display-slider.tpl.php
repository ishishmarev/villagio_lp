<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

$slide_menu = $row->field_field_slide_menu;
$slider = $row->field_field_slide;

if(!empty($row->field_field_slider_title)) {
  $slider_main_title = $row->field_field_slider_title[0]['rendered']['#markup'];
} else {
  $slider_main_title = false;
}
?>

<div id="additional__slider-<?php print $row->nid; ?>" class="additional__slider container--fluid main__menu--container <?php print !$slider_main_title ? 'no-title-slider' : ''; ?>">

  <?php if ($slider_main_title): ?>
  <div class="container">
    <h3 class="main__description--title">
      <?php print $slider_main_title; ?>
    </h3>
  </div>
  <?php endif;?>

  <div class="additional__slider--menu">
    <div class="container">
      <div class="main__menu--links">
        <ul class="clearfix">
          <?php
            foreach ($slide_menu as $slide_menu_id => $slide_menu_item) :
              $slide_menu_item_title = $slide_menu_item['rendered']['field_slide_menu_title'][0]['#markup'];
              $slide_menu_item_target = $slide_menu_item['rendered']['field_slide_menu_label'][0]['#markup'];
              $slide_menu_item_is_default = $slide_menu_item['rendered']['field_slide_menu_is_default']['#items'][0]['value'];
          ?>
          <li <?php print $slide_menu_item_is_default ? 'class="active"' : ''?>>
            <a href="#" data-target="<?php print $slide_menu_item_target; ?>"><?php print $slide_menu_item_title; ?></a>
          </li>
          <?php endforeach;?>
        </ul>
      </div>
    </div>
  </div>

  <div class="additional__slider--wrap">

    <div class="slider-pro">
      <div class="sp-slides">

        <?php
          foreach ($slider as $slider_id => $slider_item) :

            if(!empty($slider_item['rendered']['field_slide_text'])) {
              $slider_item_text = $slider_item['rendered']['field_slide_text'];
            } else {
              $slider_item_text = false;
            }

            $slider_item_image_alt = $slider_item['rendered']['field_slide_image'][0]['#item']['alt'];
            $slider_item_image_title = $slider_item['rendered']['field_slide_image'][0]['#item']['title'];

            $slider_item_image_url = file_create_url($slider_item['raw']['field_slide_image']['und'][0]['uri']);

            global $base_url;
            if (strpos($slider_item_image_url, $base_url) === 0) {
              $slider_item_image_url = '/' . ltrim(str_replace($GLOBALS['base_url'], '', $slider_item_image_url), '/');
            }

            $slider_item_target = $slider_item['rendered']['field_slide_menu_label'][0]['#markup'];

        ?>

        <div class="sp-slide" data-level="<?php print $slider_item_target; ?>">
          <a href="#feedback-form">
            <span class="slide-image" style="background-image: url('<?php print $slider_item_image_url; ?>');" alt="" title=""></span>
          </a>

          <?php if (!empty($slider_item_text)): ?>
          <div class="slider-levels__description">
            <div class="container slider-levels__description--container">
              <div class="slider-levels__body">
                <?php print $slider_item_text[0]['#markup']; ?>
              </div>
            </div>
          </div>
          <?php endif;?>

        </div>

        <?php endforeach;?>

      </div>
    </div>

  </div>

</div>