<div class="promo">
  <div class="promo__arrow promo__arrow_prev">

    <svg xmlns="http://www.w3.org/2000/svg" width="37" height="65" viewBox="0 0 37 65"><path fill="#d7d7d7" d="M36.3 61.75L33 65 0 32.5 33 0l3.3 3.25L6.6 32.5z"/><path fill="#fff" d="M36.3 61.75L33 65 0 32.5 33 0l3.3 3.25L6.6 32.5z"/></svg>
  </div>
  <div class="promo__arrow promo__arrow_next"><svg xmlns="http://www.w3.org/2000/svg" width="37" height="65" viewBox="0 0 37 65"><path fill="#d7d7d7" d="M.7 61.75L4 65l32.99-32.5L4 0 .7 3.25 30.4 32.5z"/><path fill="#fff" d="M.7 61.75L4 65l32.99-32.5L4 0 .7 3.25 30.4 32.5z"/></svg></div>
  <div class="promo__slider">
    <div class="promo__slide promo__slide-1">
      <div class="promo__container container">
        <div class="row">
          <div class="col-xs-20 col-md-10 col-lg-12">
            <div class="promo__title">Ваш дом для отдыха в 2 строки и даже в 3 строки</div>
            <div class="promo___text">Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы. Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы</div>
            <div class="promo__button button button__type-1 button__b"><a href="#">Узнать цену</a></div>
          </div>
        </div>
      </div>
    </div>
    <div class="promo__slide promo__slide-2">
      <div class="promo__container container">
        <div class="row">
          <div class="col-xs-20 col-md-10 col-lg-12">
            <div class="promo__title">Ваш дом для отдыха в 2 строки</div>
            <div class="promo___text">Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы. Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы</div>
            <div class="promo__button button button__type-1 button__b"><a href="#">Узнать цену</a></div>
          </div>
        </div>
      </div>
    </div>
    <div class="promo__slide promo__slide-3">
      <div class="promo__container container">
        <div class="row">
          <div class="col-xs-20 col-md-10 col-lg-12">
            <div class="promo__title">Ваш дом для отдыха</div>
            <div class="promo___text">Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы. Насладитесь тишиной, природой и комфортом, не уезжая далеко от Москвы</div>
            <div class="promo__button button button__type-1 button__b"><a href="#">Узнать цену</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
