<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

if ($node = menu_get_object()) {
  $landing_type = $node->field_landing_type['und'][0]['value'];
}
$slider = $fields['field_main_slider_image']->content;
$title = $fields['field_main_slider_title']->content;
$subtitle = $fields['field_main_slider_subtitle']->content;
$sale = '<div class="main__sale small-text-center">' . $fields['field_main_slider_sale']->content . '</div>';
$phone = $fields['nothing']->content;
?>

<div class="main__block--slider">
  <div class="main__slider">
    <?php print $slider; ?>
  </div>
</div>
<div class="main__slider--wrap">
  <div class="container">
    <table class="slider__wrap--table">
      <tr>
        <td class="slider__wrap--cell">
          <div class="main__block">
            <div class="row">
              <?php if (!empty($landing_type) && $landing_type == 'additional'): ?>
                <div class="col-lg-30 col-sm-30">
                  <?php print $title; ?>
                </div>
                <div class="col-lg-30 col-sm-30">
                  <?php print $subtitle; ?>
                </div>
                <div class="col-lg-15 col-sm-30">
                  <div class="main__button small-text-right">
                    <a href="#feedback-form" class="button button-extra main__button--link">Оставить заявку</a>
                  </div>
                </div>
                <div class="col-lg-15 col-sm-30">
                  <?php print $phone; ?>
                </div>
              <?php else: ?>
                <div class="col-xs-30 col-sm-18">
                  <?php print $title; ?>
                  <div class="hidden-sm visible-xs">
                    <?php print $subtitle; ?>
                    <?php if(!empty($row->field_field_main_slider_sale)): ?>
                      <?php print $sale; ?>
                    <?php endif;?>
                  </div>
                  <div class="main__button small-text-center">
                    <a href="#feedback-form" class="button button-extra main__button--link">Оставить заявку</a>
                  </div>
                  <?php print $phone; ?>
                </div>
                <div class="col-xs-30 col-sm-12 hidden-xs">
                  <?php print $subtitle; ?>

                  <?php if(!empty($row->field_field_main_slider_sale)): ?>
                    <?php print $sale; ?>
                  <?php endif;?>
                </div>
              <?php endif; ?>
            </div>
          </div>
        </td>
      </tr>
    </table>
  </div>
</div>