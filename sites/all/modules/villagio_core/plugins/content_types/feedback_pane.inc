<?php

function villagio_core_feedback_pane_ctools_content_types() {
  return array(
      'single' => TRUE,
      'title' => t('Villagio application form'),
      'description' => t('Application for feedback'),
      'category' => t('Villagio core'),
  );
}

function villagio_core_feedback_pane_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->content = drupal_get_form('villagio_core_form');
  return $block;
}

