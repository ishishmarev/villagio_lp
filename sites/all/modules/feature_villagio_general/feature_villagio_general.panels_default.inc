<?php
/**
 * @file
 * feature_villagio_general.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function feature_villagio_general_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'application';
  $mini->category = '';
  $mini->admin_title = 'Application';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'general';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'main_content' => array(
        'content' => array(
          'prefix' => '<div class="main__feedback--form">',
          'suffix' => '</div>',
        ),
        'theme' => 0,
      ),
      'default' => NULL,
    ),
    'main_content' => array(
      'style' => 'wrapper_raw',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'cd7e5564-219f-4920-8b38-e5b2226525c6';
  $display->storage_type = 'panels_mini';
  $display->storage_id = 'application';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-45a77d41-48cc-4f88-a9e6-98dbc6bd6f99';
  $pane->panel = 'main_content';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Заголовок формы',
    'title' => '',
    'title_heading' => 'h2',
    'body' => '<h3 class="main__description--title text-center">
              или оставьте заявку на обратный звонок
            </h3>
            <div class="main__description--info text-center">
              <span>
                Мы сразу же перезвоним Вам и расскажем обо всех специальных 
         предложениях
              </span>
            </div>',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'naked',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '45a77d41-48cc-4f88-a9e6-98dbc6bd6f99';
  $display->content['new-45a77d41-48cc-4f88-a9e6-98dbc6bd6f99'] = $pane;
  $display->panels['main_content'][0] = 'new-45a77d41-48cc-4f88-a9e6-98dbc6bd6f99';
  $pane = new stdClass();
  $pane->pid = 'new-bece7c4b-8bfb-4215-8872-6c33b6c2dcd6';
  $pane->panel = 'main_content';
  $pane->type = 'views_panes';
  $pane->subtype = 'view_villagio_core-view_villagio_core_display_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'naked',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'bece7c4b-8bfb-4215-8872-6c33b6c2dcd6';
  $display->content['new-bece7c4b-8bfb-4215-8872-6c33b6c2dcd6'] = $pane;
  $display->panels['main_content'][1] = 'new-bece7c4b-8bfb-4215-8872-6c33b6c2dcd6';
  $pane = new stdClass();
  $pane->pid = 'new-d56ec0aa-8a3f-4de4-bff4-688a4aaff739';
  $pane->panel = 'main_content';
  $pane->type = 'feedback_pane';
  $pane->subtype = 'feedback_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => array(
      'title' => array(
        'prefix' => '',
        'suffix' => '',
      ),
      'content' => array(
        'prefix' => '<div class="main__feedback--form-wrap">',
        'suffix' => '</div>',
      ),
      'theme' => 0,
    ),
    'style' => 'wrapper_raw',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'd56ec0aa-8a3f-4de4-bff4-688a4aaff739';
  $display->content['new-d56ec0aa-8a3f-4de4-bff4-688a4aaff739'] = $pane;
  $display->panels['main_content'][2] = 'new-d56ec0aa-8a3f-4de4-bff4-688a4aaff739';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-45a77d41-48cc-4f88-a9e6-98dbc6bd6f99';
  $mini->display = $display;
  $export['application'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'feedback';
  $mini->category = '';
  $mini->admin_title = 'Feedback';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'general';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'main_content' => array(
        'content' => array(
          'prefix' => '<div id="feedback" class="main__feedback container--fluid">
    <div class="container">
      <div class="row">',
          'suffix' => '    </div>
  </div>
</div>',
        ),
        'theme' => 0,
      ),
    ),
    'main_content' => array(
      'style' => 'wrapper_raw',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '3bb246af-4c43-4e89-8292-af11207e0e75';
  $display->storage_type = 'panels_mini';
  $display->storage_id = 'feedback';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-9ea1d0f2-56d1-428a-bf37-8bfa35aa8cd1';
  $pane->panel = 'main_content';
  $pane->type = 'views_panes';
  $pane->subtype = 'view_villagio_core-view_villagio_core_display_manager';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => array(
      'title' => array(
        'prefix' => '',
        'suffix' => '',
      ),
      'content' => array(
        'prefix' => '<div class="main__feedback--col col-xs-30 col-sm-12 small-text-center">',
        'suffix' => '</div>',
      ),
      'theme' => 0,
    ),
    'style' => 'wrapper_raw',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9ea1d0f2-56d1-428a-bf37-8bfa35aa8cd1';
  $display->content['new-9ea1d0f2-56d1-428a-bf37-8bfa35aa8cd1'] = $pane;
  $display->panels['main_content'][0] = 'new-9ea1d0f2-56d1-428a-bf37-8bfa35aa8cd1';
  $pane = new stdClass();
  $pane->pid = 'new-5a74cead-3f6d-41f8-ae34-0a0c0bf78894';
  $pane->panel = 'main_content';
  $pane->type = 'panels_mini';
  $pane->subtype = 'application';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => array(
      'title' => array(
        'prefix' => '',
        'suffix' => '',
      ),
      'content' => array(
        'prefix' => '<div id="feedback-form" class="main__feedback--col col-xs-30 col-sm-18">',
        'suffix' => '</div>',
      ),
      'theme' => 0,
    ),
    'style' => 'wrapper_raw',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '5a74cead-3f6d-41f8-ae34-0a0c0bf78894';
  $display->content['new-5a74cead-3f6d-41f8-ae34-0a0c0bf78894'] = $pane;
  $display->panels['main_content'][1] = 'new-5a74cead-3f6d-41f8-ae34-0a0c0bf78894';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['feedback'] = $mini;

  return $export;
}
