<?php
/**
 * @file
 * feature_villagio_general.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_villagio_general_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "multifield" && $api == "") {
    return array("version" => "");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function feature_villagio_general_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function feature_villagio_general_node_info() {
  $items = array(
    'application' => array(
      'name' => t('Application'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'landing' => array(
      'name' => t('Лендинг'),
      'base' => 'node_content',
      'description' => t('Создание лендинга для нового дома.'),
      'has_title' => '1',
      'title_label' => t('Страница '),
      'help' => '',
    ),
    'landing_code' => array(
      'name' => t('Landing (code)'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Наименование'),
      'help' => '',
    ),
    'slider' => array(
      'name' => t('Слайдер'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Название слайдера для появляющегося меню'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'yamap' => array(
      'name' => t('Карта'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Название лендинга к которому относится'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
