<?php
/**
 * @file
 * feature_villagio_general.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_villagio_general_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_advantage|node|landing|form';
  $field_group->group_name = 'group_advantage';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'landing';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Преимущества',
    'weight' => '13',
    'children' => array(
      0 => 'field_advantage_title',
      1 => 'field_advantage_subtitle',
      2 => 'field_advantage_set',
      3 => 'field_advantage_subtitle_link',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Преимущества',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-advantage field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_advantage|node|landing|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_documents|node|landing|form';
  $field_group->group_name = 'group_documents';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'landing';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Документация',
    'weight' => '15',
    'children' => array(
      0 => 'field_documents',
      1 => 'field_documents_subtitle',
      2 => 'field_documents_title',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Документация',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-documents field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_documents|node|landing|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_form|node|landing|form';
  $field_group->group_name = 'group_form';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'landing';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Форма',
    'weight' => '18',
    'children' => array(
      0 => 'field_form_title',
      1 => 'field_form_description',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-form field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_form|node|landing|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_home_list|node|landing|form';
  $field_group->group_name = 'group_home_list';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'landing';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Список домов',
    'weight' => '19',
    'children' => array(
      0 => 'field_home_list_description',
      1 => 'field_home_list_image',
      2 => 'field_home_list_title',
      3 => 'field_home_list_main_title',
      4 => 'field_home_list_set_list',
      5 => 'field_home_list_price',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-home-list field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_home_list|node|landing|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_main_slider|node|landing|form';
  $field_group->group_name = 'group_main_slider';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'landing';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Основной слайдер',
    'weight' => '12',
    'children' => array(
      0 => 'field_main_slider_image',
      1 => 'field_main_slider_sale',
      2 => 'field_main_slider_title',
      3 => 'field_main_slider_subtitle',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Основной слайдер',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_main_slider|node|landing|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_manager|node|landing|form';
  $field_group->group_name = 'group_manager';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'landing';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Блок менеджера',
    'weight' => '16',
    'children' => array(
      0 => 'field_manager_title',
      1 => 'field_manager_description',
      2 => 'field_manager_photo',
      3 => 'field_manager_email',
      4 => 'field_manager_phone',
      5 => 'field_manager_name',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Блок менеджера',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-manager field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_manager|node|landing|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_payments|node|landing|form';
  $field_group->group_name = 'group_payments';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'landing';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Оплата',
    'weight' => '14',
    'children' => array(
      0 => 'field_payments_method',
      1 => 'field_payments_subtitle',
      2 => 'field_payments_title',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Оплата',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-payments field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_payments|node|landing|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Блок менеджера');
  t('Документация');
  t('Оплата');
  t('Основной слайдер');
  t('Преимущества');
  t('Список домов');
  t('Форма');

  return $field_groups;
}
