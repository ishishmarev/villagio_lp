<?php
/**
 * @file
 * feature_villagio_general.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function feature_villagio_general_default_rules_configuration() {
  $items = array();
  $items['rules_send_mail_after_create_application'] = entity_import('rules_config', '{ "rules_send_mail_after_create_application" : {
      "LABEL" : "Send mail after create application",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "application" ],
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--application" : { "bundle" : "application" } },
      "DO" : [
        { "mail" : {
            "to" : "[node:field-email-form]",
            "subject" : "\\u0417\\u0430\\u044f\\u0432\\u043a\\u0430 \\u043d\\u0430 \\u043e\\u0431\\u0440\\u0430\\u0442\\u043d\\u044b\\u0439 \\u0437\\u0432\\u043e\\u043d\\u043e\\u043a",
            "message" : "\\u041f\\u043e\\u0441\\u0442\\u0443\\u043f\\u0438\\u043b\\u0430 \\u0437\\u0430\\u044f\\u0432\\u043a\\u0430 \\u043d\\u0430 \\u043e\\u0431\\u0440\\u0430\\u0442\\u043d\\u044b\\u0439 \\u0437\\u0432\\u043e\\u043d\\u043e\\u043a.\\r\\n\\u0421\\u0430\\u0439\\u0442: [site:url]\\r\\n\\u041f\\u043e\\u0441\\u0451\\u043b\\u043e\\u043a + \\u043d\\u043e\\u043c\\u0435\\u0440 \\u0434\\u043e\\u043c\\u0430: [node:field-home-id]\\r\\n\\u0418\\u043c\\u044f: [node:title]\\r\\n\\u0422\\u0435\\u043b\\u0435\\u0444\\u043e\\u043d: [node:field-client-phone]\\r\\n\\u0414\\u043b\\u044f \\u0430\\u0433\\u0435\\u043d\\u0442\\u0430: [node:field-manager]\\r\\n\\r\\n\\u0421\\u0441\\u044b\\u043b\\u043a\\u0430 \\u043f\\u0435\\u0440\\u0435\\u0445\\u043e\\u0434\\u0430 \\u043d\\u0430 \\u0441\\u0430\\u0439\\u0442: [node:field-links-data]",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
