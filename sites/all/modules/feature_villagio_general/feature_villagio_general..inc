<?php
/**
 * @file
 * feature_villagio_general..inc
 */

/**
 * Implements hook_multifield_default_multifield().
 */
function feature_villagio_general_multifield_default_multifield() {
  $export = array();

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_advantage_set';
  $multifield->label = 'field_advantage_set';
  $multifield->description = '';
  $export['field_advantage_set'] = $multifield;

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_documents';
  $multifield->label = 'field_documents';
  $multifield->description = '';
  $export['field_documents'] = $multifield;

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_payments_method';
  $multifield->label = 'field_payments_method';
  $multifield->description = '';
  $export['field_payments_method'] = $multifield;

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_slide';
  $multifield->label = 'field_slide';
  $multifield->description = '';
  $export['field_slide'] = $multifield;

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->machine_name = 'field_slide_menu';
  $multifield->label = 'field_slide_menu';
  $multifield->description = '';
  $export['field_slide_menu'] = $multifield;

  return $export;
}
